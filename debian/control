Source: objfw
Maintainer: Jonathan Schleifer <js@nil.im>
Uploaders: Alex Myczko <tar@debian.org>
Section: misc
Priority: optional
Standards-Version: 4.7.0
Homepage: https://objfw.nil.im/
Build-Depends: debhelper-compat (= 13), clang [!alpha !hppa !hurd-amd64 !ia64
 !m68k !sh4 !x32], gobjc [alpha hppa hurd-amd64 ia64 m68k sh4 x32], gnutls-dev, 
 pkgconf
Vcs-Browser: https://salsa.debian.org/debian/objfw
Vcs-Git: https://salsa.debian.org/debian/objfw.git

Package: objfw
Architecture: any
Depends: libobjfw1 (= ${binary:Version}), libobjfw1-dev (= ${binary:Version}),
 libobjfwrt1 (= ${binary:Version}), libobjfwrt1-dev (= ${binary:Version}),
 libobjfwtls1 (= ${binary:Version}), libobjfwtls1-dev (= ${binary:Version}),
 ofarc (= ${binary:Version}), ofdns (= ${binary:Version}),
 ofhash (= ${binary:Version}), ofhttp (= ${binary:Version})
Description: Portable, lightweight framework for the Objective-C language
 ObjFW is a portable, lightweight framework for the Objective-C language. It
 enables you to write an application in Objective-C that will run on any
 platform supported by ObjFW without having to worry about differences between
 operating systems or various frameworks you would otherwise need if you want to
 be portable.
 .
 It supports all modern Objective-C features when using Clang, but is also
 compatible with GCC ≥ 4.6 to allow maximum portability.
 .
 ObjFW also comes with its own lightweight and extremely fast Objective-C
 runtime, which in real world use cases was found to be significantly faster
 than both GNU's and Apple's runtime.

Package: libobjfw1
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libobjfwrt1 (= ${binary:Version})
Description: ObjFW library
 The libobjfw1 package contains the library needed by programs using ObjFW.

Package: libobjfw1-dev
Provides: libobjfw-dev
Conflicts: libobjfw-dev
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libobjfw1 (= ${binary:Version}),
 libobjfwrt1-dev (= ${binary:Version}), libobjfwhid1-dev (= ${binary:Version})
Description: Header files, libraries and tools for libobjfw1
 The libobjfw1-dev package contains the header files, libraries and tools to
 develop programs using ObjFW.

Package: libobjfwrt1
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: ObjFW Objective-C runtime library
 The libobjfwrt1 package contains ObjFW's Objective-C runtime library.

Package: libobjfwrt1-dev
Provides: libobjfwrt-dev
Conflicts: libobjfwrt-dev
Architecture: any
Depends: libobjfwrt1 (= ${binary:Version})
Description: Header files and libraries for libobjfwrt
 The libobjfwrt1-dev package contains header files and libraries for ObjFW's
 Objective-C runtime library.

Package: libobjfwtls1
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libobjfw1 (= ${binary:Version}),
 libobjfwrt1 (= ${binary:Version})
Description: TLS support for ObjFW
 The libobjfwtls1 package contains TLS support for ObjFW.

Package: libobjfwtls1-dev
Provides: libobjfwtls-dev
Conflicts: libobjfwtls-dev
Architecture: any
Depends: libobjfwtls1 (= ${binary:Version}),
 libobjfw1-dev (= ${binary:Version}), libobjfwrt1-dev (= ${binary:Version})
Description: Header files and libraries for libobjfwtls
 The libobjfwtls1-dev package contains header files and libraries for TLS
 support for ObjFW.

Package: libobjfwhid1
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libobjfw1 (= ${binary:Version}),
 libobjfwrt1 (= ${binary:Version})
Description: HID support for ObjFW
 The libobjfwhid1 package contains HID support for ObjFW.

Package: libobjfwhid1-dev
Provides: libobjfwhid-dev
Conflicts: libobjfwhid-dev
Architecture: any
Depends: libobjfwhid1 (= ${binary:Version}),
 libobjfw1-dev (= ${binary:Version}), libobjfwrt1-dev (= ${binary:Version})
Description: Header files and libraries for libobjfwhid
 The libobjfwhid1-dev package contains header files and libraries for HID
 support for ObjFW.

Package: ofarc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libobjfw1 (= ${binary:Version}),
 libobjfwrt1 (= ${binary:Version}), libobjfwtls1 (= ${binary:Version})
Description: Utility for handling ZIP, Tar and LHA archives
 ofarc is a multi-format archive utility that allows creating, listing,
 extracting and modifying ZIP, Tar and LHA archives using ObjFW's classes for
 various archive types.

Package: ofdns
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libobjfw1 (= ${binary:Version}),
 libobjfwrt1 (= ${binary:Version})
Description: Utility for performing DNS requests on the command line
 ofdns is an utility for performing DNS requests on the command line using
 ObjFW's DNS resolver.

Package: ofhash
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libobjfw1 (= ${binary:Version}),
 libobjfwrt1 (= ${binary:Version}), libobjfwtls1 (= ${binary:Version})
Description: Utility to hash files with various cryptographic hash functions
 ofhash is an utility to hash files with various cryptographic hash functions
 (even using different algorithms at once) using ObjFW's classes for various
 cryptographic hashes.

Package: ofhttp
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libobjfw1 (= ${binary:Version}),
 libobjfwrt1 (= ${binary:Version}), libobjfwtls1 (= ${binary:Version})
Description: Command line downloader for HTTP(S)
 ofhttp is a command line downloader for HTTP and HTTPS using ObjFW's
 OFHTTPClient class. It supports all features one would expect from a modern
 command line downloader such as resuming of downloads, using a SOCKS5 proxy, a
 modern terminal-based UI, etc.
